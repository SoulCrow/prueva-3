/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convocatoria;

import java.io.Serializable;


public class Trabajador implements Serializable {
    
    protected String Nombres;
    protected String Apellidos;
    protected String Codigo;
    protected String Categoria;
    protected int Bonificacion;
    protected int Salario;
    protected String Oficina;

    public String getOficina() {
        return Oficina;
    }

    public void setOficina(String Oficina) {
        this.Oficina = Oficina;
    }

    public int getSalario() {
        return Salario;
    }

    public void setSalario(int Salario) {
        this.Salario = Salario;
    }

    public Trabajador(String Nombres, String Apellidos, String Codigo, String Categoria, int Bonificacion) {
        this.Nombres = Nombres;
        this.Apellidos = Apellidos;
        this.Codigo = Codigo;
        this.Categoria = Categoria;
        this.Bonificacion = Bonificacion;
    }
    
    public Trabajador(){
        
    }
    
    

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }

    public String getCategoria() {
        return Categoria;
    }

    public void setCategoria(String Categoria) {
        this.Categoria = Categoria;
    }

    public int getBonificacion() {
        return Bonificacion;
    }

    public void setBonificacion(int Bonificacion) {
        this.Bonificacion = Bonificacion;
    }
    
    
}
