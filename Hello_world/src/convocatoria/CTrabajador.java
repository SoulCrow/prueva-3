/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convocatoria;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;


public class CTrabajador extends Trabajador {
    
    public CTrabajador(){
        
    }
    
    private ArrayList<Trabajador> trabajador = new ArrayList<Trabajador>();

    public ArrayList<Trabajador> getTrabajador() {
        return trabajador;
    }

    public void setTrabajador(ArrayList<Trabajador> trabajador) {
        this.trabajador = trabajador;
    }
    
        public ArrayList<Trabajador> Agregar(){
        Scanner s1 = new Scanner(System.in);
        Scanner s2 = new Scanner(System.in);
        Trabajador t = new Trabajador();
       
        System.out.println("Ingrese el codigo del Trabajador: ");
        t.setCodigo(s2.nextLine());
        System.out.println("Ingrese los nombres: ");
        t.setNombres(s2.nextLine());
        System.out.println("Ingrese los apellido: ");
        t.setApellidos(s2.nextLine());
        System.out.println("Ingrese el Salario: ");
        t.setSalario(s1.nextInt());
        System.out.println("Ingrese la oficina: ");
        t.setOficina(s2.nextLine());
        System.out.println("Ingrese la categoria: ");
        t.setCategoria(s2.nextLine());
        System.out.println("Ingrese la bonificacion: ");
        t.setBonificacion(s1.nextInt());
        
        trabajador.add(t);
        this.Serializacion();
        this.Deserizalizacion();
        
        return trabajador;
    }
        
    public void totalPorOficina(){
        int merc = 0;
        int finanzas = 0;
        int ventas = 0;
        int cont = 0;
        for(Trabajador t : trabajador){
            if(t.getOficina().equals("Mercadotecnia") || t.getOficina().equals("MERCADOTECNIA"))
            {
                merc += t.getSalario();
            }
            else if(t.getOficina().equals("Finanzas") || t.getOficina().equals("FINANZAS"))
            {
                finanzas += t.getSalario();
            }
            
            else if(t.getOficina().equals("Ventas") || t.getOficina().equals("VENTAS"))
            {
                ventas += t.getSalario();
            }
            
            else if(t.getOficina().equals("Contabilidad") || t.getOficina().equals("CONTABILIDAD"))
            {
                cont += t.getSalario();
            }
            }
        
        System.out.println("Total oficina Mercadotecnia: "+merc);
        System.out.println("Total oficina Finanzas: "+finanzas);
        System.out.println("Total oficina Ventas: "+ventas);
        System.out.println("Total oficina Contabilidad: "+cont);
    }
        
    public void verCategoriaSalario(){
            for(Trabajador t : trabajador){
            if(t.getSalario() > 1500 && t.getBonificacion() > 8 && t.getCategoria().equals("A")){
                System.out.println(""+t.getNombres()+" / "+t.getApellidos()+" / "+t.getCodigo()+" / "
                +t.getCategoria()+" / "+t.getSalario()+" / "+t.getBonificacion());
            }
            
            
        }
    }
    
        public void verSalarioBonificacionCt(){
            for(Trabajador t : trabajador){
            if(t.getOficina().equals("Contabilidad") || t.getOficina().equals("CONTABILIDAD")){
                System.out.println(""+t.getNombres()+" / "+t.getApellidos()+" / "+t.getCodigo()+" / "
                +t.getCategoria()+" / "+t.getSalario()+" / "+t.getBonificacion());
            }
            
            
        }         
        
    }
        public void ver(){
            for(Trabajador t : trabajador){
    
            System.out.println(""+t.getNombres()+" / "+t.getApellidos()+" / "+t.getCodigo()+" / "
            +t.getCategoria()+" / "+t.getSalario()+" / "+t.getBonificacion());
            
            }
    }
        
        public void verPromedioSalarioB(){
            int total = 0;
            for(Trabajador t : trabajador){
    
            total += t.getSalario() + t.getBonificacion();
            
            }
            
            float prom = ((float)total)/((float)this.getTrabajador().size());
            System.out.println("El promedio del salario con bonificaicon de la empresa es: "+prom);
    }
        
        public void verPromedioSalario(){
            int total = 0;
            for(Trabajador t : trabajador){
                total += t.getSalario();
            
            }
            
            float promedio = ((float)total)/((float)this.getTrabajador().size());
            System.out.println("El promedio del salario de la empresa es: "+promedio);
    }
        
        public void verTotalBonificacionFinanzas(){
            int totalB  = 0;
            for(Trabajador t : trabajador){
                if(t.getOficina().equals("Finanzas") || t.getOficina().equals("FINANZAS")){
                    totalB = t.getBonificacion();
                }
                
                System.out.println("El total de la bonificacion en finanzas es: "+totalB);
            
            }
    }
        public void verPorCategoria(){
            Scanner s = new Scanner(System.in);
            System.out.println("Ingrese la categoria que desea ver: ");
            String Categoria = s.nextLine();
            for(Trabajador t : trabajador){
            if(Categoria.equals(t.getCategoria())){
                System.out.println(""+t.getNombres()+" / "+t.getApellidos()+" / "+t.getCategoria());
            }
            
            
            }
        }
        
        public void verSalarioPorCategoria(){
            Scanner s = new Scanner(System.in);
            System.out.println("Ingrese la categoria que desea ver: ");
            String Categoria = s.nextLine();
            for(Trabajador t : trabajador){
            if(Categoria.equals(t.getCategoria())){
                System.out.println(""+t.getNombres()+" / "+t.getApellidos()+" / "+t.getSalario()+" / "+t.getCategoria());
            }
            
            
            }
        }
    
        public void Serializacion(){
        FileOutputStream fos = null;
        ObjectOutputStream salida = null;
        
        try {
            fos = new FileOutputStream("BDD.dat");
            salida = new ObjectOutputStream(fos);
            salida.writeObject(this.getTrabajador());
        } 
        catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        } 
        finally{
            try {
                if (fos != null) {
                    fos.close();
                }
                if (salida != null) {
                    salida.close();
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
        
    }

    public void Deserizalizacion(){
        FileInputStream fis = null;
        ObjectInputStream entrada = null;

        try {
            fis = new FileInputStream("BDD.dat");
            entrada = new ObjectInputStream(fis);
            trabajador = (ArrayList<Trabajador>) entrada.readObject();
        } 
        catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
        catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        } 
        catch (IOException e) {
            System.out.println(e.getMessage());
        } 
        finally{
            try {
                if (fis != null) {
                    fis.close();
                }
                if (entrada != null) {
                    entrada.close();
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }

    }
        
        
}
